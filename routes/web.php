<?php


Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/', 'HomeController@index')->name('index');
Route::get('/home', 'HomeController@index')->name('home');


Route::prefix('/cooperated')->group(function(){

    Route::get('/', 'CooperatedController@index')->name('cooperated.index');
    Route::get('/create', 'CooperatedController@create')->name('cooperated.create');
    Route::post('/create', 'CooperatedController@store')->name('cooperated.create');
    Route::get('/{id}', 'CooperatedController@show')->name('cooperated.show');
    Route::get('/edit/{id}', 'CooperatedController@edit')->name('cooperated.edit');
    Route::get('/trash', 'CooperatedController@trash')->name('cooperated.trash');
    Route::get('/delete/{id}', 'CooperatedController@delete')->name('cooperated.delete');
    Route::get('/restore/{id}', 'CooperatedController@restore')->name('cooperated.restore');
    Route::get('/destroy/{id}', 'CooperatedController@destroy')->name('cooperated.destroy');
    Route::post('/update/{id}', 'CooperatedController@update')->name('cooperated.update');

    Route::get('/addbank/{id}', 'BankDataController@showaddbank')->name('cooperated.addbank');
    Route::post('/addbank/{id}', 'BankDataController@storebank')->name('cooperated.addbank');
});

Route::prefix('/buyers')->group(function(){

    Route::get('/', 'BuyerController@index')->name('buyer.index');
    Route::get('/negative/{id}', 'BuyerController@negative')->name('buyer.negative');
    Route::get('/positive/{id}', 'BuyerController@positive')->name('buyer.positive');
    Route::get('/negativeds', 'BuyerController@negativeds')->name('buyer.negativeds');
    Route::get('/create', 'BuyerController@create')->name('buyer.cretae');
    Route::post('/create', 'BuyerController@store')->name('buyer.create');
    Route::get('/show/{id}', 'BuyerController@show')->name('buyer.show');
    Route::get('/edit/{id}', 'BuyerController@edit')->name('buyer.edit');
    Route::post('/update/{id}', 'BuyerController@update')->name('buyer.update');
    Route::get('/trash', 'BuyerController@trash')->name('buyer.trash');
    Route::get('/delete/{id}', 'BuyerController@delete')->name('buyer.delete');
    Route::get('/restore/{id}', 'BuyerController@restore')->name('buyer.restore');
    Route::get('/destroy/{id}', 'BuyerController@destroy')->name('buyer.destroy');

    Route::get('/addbank/{id}', 'BankDataController@showaddbank')->name('buyer.addbank');
    Route::post('/addbank/{id}', 'BankDataController@storebank')->name('buyer.addbank');
});

Route::prefix('/stock')->group(function(){
    Route::get('/', 'StockController@index')->name('stock.index');
    Route::get('/trash', 'StockController@trash')->name('stock.trash');
    Route::get('/addstock/{id}', 'StockController@addstock')->name('stock.addstock');
    Route::post('/addstock/{id}', 'StockController@storestock')->name('stock.addstock');
    Route::get('/edit/{id}', 'StockController@showeditstock')->name('stock.editstock');
    Route::post('/edit/{id}', 'StockController@editstock')->name('stock.editstock');
    Route::get('/delete/{id}', 'StockController@delete')->name('stock.delete');
    Route::get('/destroy/{id}', 'StockController@destroy')->name('stock.destroy');
    Route::get('/restore/{id}', 'StockController@restore')->name('stock.restore');
});

Route::prefix('/users')->group(function(){
    Route::get('/', 'UserController@index')->name('user.index');
    Route::get('/create', 'UserController@create')->name('user.create');
    Route::post('/create', 'UserController@store')->name('user.create');
    Route::get('/show/{id}', 'UserController@show')->name('user.show');
    Route::get('/edit/{id}', 'UserController@edit')->name('user.edit');
    Route::post('/edit/{id}', 'UserController@update')->name('user.update');
    Route::get('/trash', 'UserController@trash')->name('user.trash');
    Route::get('/delete/{id}', 'UserController@delete')->name('user.delete');
    Route::get('/destroy/{id}', 'UserController@destroy')->name('user.destroy');
    Route::get('/restore/{id}', 'UserController@restore')->name('user.restore');
    
});

Route::prefix('/sales')->group(function(){
    Route::get('/', 'SaleController@index')->name('sale.index');
    Route::get('/create', 'SaleController@create')->name('sale.create');
    Route::post('/create', 'SaleController@store')->name('sale.create');
    Route::get('/show/{id}', 'SaleController@show')->name('sale.show');
    Route::get('/edit/{id}', 'SaleController@edit')->name('sale.edit');
    Route::post('/edit/{id}', 'SaleController@update')->name('sale.update');
    Route::get('/trash', 'SaleController@trash')->name('sale.trash');
    Route::get('/delete/{id}', 'SaleController@delete')->name('sale.delete');
    Route::get('/destroy/{id}', 'SaleController@destroy')->name('sale.destroy');
    Route::get('/restore/{id}', 'SaleController@restore')->name('sale.restore');
    Route::get('/getb', 'SaleController@getb')->name('sale.getb');
    
});

