$(document).ready(function(){
    
    $('.toggle-nav').click(function(e){
        e.preventDefault();        
        toggleNav();        
    });
    
    function toggleNav(){
        
        if(!$('#wrapper').hasClass('show-nav')){
            $('#wrapper').addClass('show-nav');
        } else {
            $('#wrapper').removeClass('show-nav');
        }
        
    }
});