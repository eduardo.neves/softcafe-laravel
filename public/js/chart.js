$(document).ready(function () {

    Highcharts.chart('container', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Relatorio de produtividade 2018'
        },
        xAxis: {
            categories: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
        },
        yAxis: {
            title: {
                text: 'Vendas R$'
            }
        },
        plotOptions: {
            line: {
                enableMouseTracking: false
            }
        },
        series: [{
                name: 'Vendas 2018',
                data: [70, 66, 94, 144, 184, 215, 255, 265, 235, 300, 356, 699]
            }]
    });

});