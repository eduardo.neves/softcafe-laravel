$(document).ready(function () {

    $('.dataTable').dataTable({
        "order": [[0, "desc"]],
        "language": {
            "lengthMenu": "Mostrando _MENU_ resultados por página",
            "zeroRecords": "Nada encontrado",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum resultado encontrado",
            "infoFiltered": "(filtrado de _MAX_ total resultados)",
            "paginate": {
                "previous": "Anterior",
                "next": "Proximo"
            },
            "search": "Pesquisar"
        },
        "pageLength": 5
    });

    $('.btn-menu-toggle, .menu .menu-container').click(function (e) {

        e.preventDefault();

        $('.menu-main').toggleClass('showmenu');

        if ($('.menu-main').hasClass('showmenu')) {

            $('.menu-container').fadeIn('fast');

        } else {

            $('.menu-container').fadeOut('fast');
        }

    });
    $('.menu .ancor').click(function (e) {

        e.preventDefault();

        $(this).next('ul').addClass('open');

    });

    $('.btn-close').click(function (e) {

        e.preventDefault();

        $(this).parent('ul').removeClass('open');

    });

    $('.dataTable').show();
    
    $('.bags').keyup(function(){
        
        $('.total').val('R$' + parseFloat($(this).val() * 425));
        $('.sale_value').val(parseFloat($(this).val() * 425));
        
    });
    
    $(document).on("click", ".venda a", function(e){
       e.preventDefault();
       
       var text = $(this).text();
       
       $('.buyer').val(text);
       
       $('.venda a').remove();
    });

    $('.buyer').keyup(function () {

        $.ajax({
            url: '/sales/getb',
            type: 'get',
            data: {
                search: $(this).val(),
            },
            success: function (response) {

                var out = $('.venda');
                out.children('a').remove();

                response.forEach(function (key, value) {
                    var a = document.createElement('a');
                    var node = document.createTextNode(key['name']);
                    a.appendChild(node);
                    out.append(a);
                });

            },
            error: function () {
                console.log('erro')
            }
        });
    });
    
    

});