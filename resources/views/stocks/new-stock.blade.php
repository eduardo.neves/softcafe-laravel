@extends('layouts.admin')

@section('content')

<h1>Cadastrar novo estoque</h1>

<div class="row">
    <h2 class="ds-table fl-left">Cooperado: {{ $cooperado->name }}</h2>

    <a href="{{ route('cooperated.index') }}" class="fl-right btn-back"><i class="fa fa-arrow-left"></i>voltar</a>
</div>

@if(session()->has('message'))
<div class="alert alert-success">
    <i class="fa fa-info-circle"></i> {{ session()->get('message') }}
</div>
@endif

<form action="{{ route('stock.addstock', $cooperado->id) }}" method="POST">
    
    @csrf
    
    <input type="hidden" name="cooperated_id" value="{{ $cooperado->id }}">        
    
    <div class="formgroup {{ $errors->has('name') ? ' is-invalid' : '' }}">
        
        <label>NUMERO DE SACAS</label>
        <input type="number" name="bags" value="{{ old('bags') }}" required="" placeholder="Ex. 599" >
        
        @if ($errors->has('bags'))
        <span>
            <strong>{{ $errors->first('bags') }}</strong>
        </span>
        @endif
    </div>
    
    <div class="formgroup {{ $errors->has('name') ? ' is-invalid' : '' }}">
        
        <label>TIPO DE CAFÉ</label>
        <select name="type" required="">
            <option value="1">TIPO 1</option>
            <option value="2">TIPO 2</option>
        </select>
        
        @if ($errors->has('type'))
        <span>
            <strong>{{ $errors->first('type') }}</strong>
        </span>
        @endif
    </div>
    
    <div class="formgroup">
        
        <button class="btn" type="submit">CADASTRAR</button>
    </div>
    
</form>

@endsection