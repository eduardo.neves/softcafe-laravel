@extends('layouts.admin')

@section('content')

<h1>Stoques (SC Café)</h1>


@if(session()->has('message'))
<div class="alert alert-success">
    <i class="fa fa-info-circle"></i> {{ session()->get('message') }}
</div>
@endif

@if(session()->has('danger'))
<div class="alert alert-warning">
    <i class="fa fa-warning"></i> {{ session()->get('danger') }}
</div>
@endif

<table class="dataTable striped">

    <thead>
        <tr>
            <th>COOPERADO</th>
            <th>QTD</th>
            <th>TIPO</th>
            <th>DATA</th>
            <th></th>
        </tr>

    </thead>

    <tbody>

        @foreach($stocks as $stock)
        <tr>
            <td>{{ $stock->cooperated->name }}</td>
            <td>{{ $stock->bags }}</td>
            <td>{{ $stock->type }}</td>
            <td>{{ date('d-m-Y h:i:s', strtotime($stock->created_at)) }}</td>
            <td>
                <a href="{{ route('stock.delete', $stock->id) }}" class="btn btn-warning"><i class="fa fa-trash"></i></a>
                <a href="{{ route('stock.editstock', $stock->cooperated->id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
            </td>
        </tr>
        @endforeach

    </tbody>
</table>

@endsection