@extends('layouts.admin')

@section('content')

<h1>Lixeira (Stoque)</h1>


@if(session()->has('message'))
<div class="alert alert-success">
    <i class="fa fa-info-circle"></i> {{ session()->get('message') }}
</div>
@endif

@if(session()->has('danger'))
<div class="alert alert-warning">
    <i class="fa fa-warning"></i> {{ session()->get('danger') }}
</div>
@endif

<table class="dataTable striped">

    <thead>
        <tr>
            <th>COOPERADO</th>
            <th>QTD</th>
            <th>TIPO</th>
            <th>DATA</th>
            <th></th>
        </tr>

    </thead>

    <tbody>

        @foreach($stocks as $stock)
        <tr>
            <td>{{ $stock->cooperated->name }}</td>
            <td>{{ $stock->bags }}</td>
            <td>{{ $stock->type }}</td>
            <td>{{ date('d-m-Y h:i:s', strtotime($stock->created_at)) }}</td>
            <td>
                <a href="{{ route('stock.destroy', $stock->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                <a href="{{ route('stock.restore', $stock->id) }}" class="btn btn-success"><i class="fa fa-refresh"></i></a>
            </td>
        </tr>
        @endforeach

    </tbody>
</table>

@endsection