@extends('layouts.admin')

@section('content')

<h1>Lixeira (Compradores)</h1>

@if(session()->has('message'))
<div class="alert alert-success">
    <i class="fa fa-info-circle"></i> {{ session()->get('message') }}
</div>
@endif

<table class="dataTable striped">

    <thead>
        <tr>
            <th>NOME</th>
            <th>EMAIL</th>
            <th>CNPJ</th>
            <th>TELEFONE</th>
            <th></th>
        </tr>

    </thead>

    <tbody>

        @foreach($buyers as $buyer)
        <tr>
            <td class="table-link"><a href="{{ route('buyer.show', $buyer->id) }}">{{ $buyer->name }}</a></td>
            <td>{{ ($buyer->email == null ? 'Sem email' : $buyer->email) }}</td>
            <td>{{ $buyer->cnpj }}</td>
            <td>{{ $buyer->phone_number }}</td>
            
            <td>
                <a href="{{ route('buyer.destroy', $buyer->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                <a href="{{ route('buyer.restore', $buyer->id) }}" class="btn btn-success"><i class="fa fa-refresh"></i></a>
            </td>
        </tr>
        @endforeach

    </tbody>
</table>
@endsection