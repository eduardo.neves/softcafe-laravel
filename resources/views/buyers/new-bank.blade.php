@extends('layouts.admin')

@section('content')

<h1>Cadastrar dados bancarios</h1>

<h2>Cooperado: {{ $cooperado->name }}</h2>

@if(session()->has('message'))
<div class="alert alert-success">
    <i class="fa fa-info-circle"></i> {{ session()->get('message') }}
</div>
@endif

<form action="{{ route('cooperated.addbank', $cooperado->id) }}" method="POST">
    
    @csrf
    
    <input type="hidden" name="cooperated_id" value="{{ $cooperado->id }}">    
    
    <div class="formgroup">
        <label>BANCO</label>
        
        <input type="text" name="bank" placeholder="Ex. Banco do Brasil" required="">
    </div>
    
    <div class="formgroup">
        <label>AGÊNCIA</label>
        
        <input type="text" name="agency" placeholder="Ex. 8888" required="">
    </div>
    
    <div class="formgroup">
        <label>CONTA</label>
        
        <input type="text" name="account" placeholder="Ex. 8888-8" required="">
    </div>
    
    <div class="formgroup">
        <label>TIPO</label>
        
        <select name="account_type">
            <option value="cc">Conta corrente</option>
            <option value="pp">Poupança</option>
            <option value="ss">Salario</option>
            <option value="ff">Fácil</option>
        </select>
    </div>
    
    <div class="formgroup">
        <button class="btn" type="submit">CADASTRAR</button>
    </div>
    
</form>

@endsection