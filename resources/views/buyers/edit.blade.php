@extends('layouts.admin')

@section('content')

<h1>Editar comprador</h1>

@if(session()->has('message'))
<div class="alert alert-success">
    <i class="fa fa-info-circle"></i> {{ session()->get('message') }}
</div>
@endif

<form action="{{ route('buyer.update', $comprador->id) }}" method="POST" enctype="multipart/form-data">

    @csrf

    <div class="formgroup {{ $errors->has('name') ? ' is-invalid' : '' }}">
        <label>NOME COMPLETO</label>
        <input type="text" name="name" value="{{ old('name') ?? $comprador->name }}" required="" placeholder="Nome completo" max="255" autofocus="">

        @if ($errors->has('name'))
        <span>
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>

    <div class="formgroup {{ $errors->has('address') ? ' is-invalid' : '' }}">
        <label>ENDEREÇO</label>
        <input type="text" name="address" value="{{ old('address') ?? $comprador->address }}" required="" placeholder="Ex. Rua Toledo, 20, Fatima, Belem" max="255">

        @if ($errors->has('address'))
        <span>
            <strong>{{ $errors->first('address') }}</strong>
        </span>
        @endif
    </div>

    <div class="row">

        <div class="formgroup col col-g spacing {{ $errors->has('city') ? ' is-invalid' : '' }}">
            <label>CIDADE</label>
            
            <select name="city" required="">
                <option value="Belem" {{ $comprador->city == 'Belem' ? 'selected' : '' }}>Belem</option>
            </select>

            @if ($errors->has('city'))
            <span>
                <strong>{{ $errors->first('city') }}</strong>
            </span>
            @endif
        </div>
        
        <div class="formgroup col col-g spacing {{ $errors->has('neighborhood') ? ' is-invalid' : '' }}">
            <label>BAIRRO</label>
            
            <select name="neighborhood" required="">
                <option value="Marco" {{ $comprador->city == 'Marco' ? 'selected' : '' }}>Marco</option>
                <option value="Pedreira" {{ $comprador->city == 'Pedreira' ? 'selected' : '' }}>Pedreira</option>
                <option value="Telegrafo" {{ $comprador->city == 'Telegrafo' ? 'selected' : '' }}>Telegrafo</option>
            </select>

            @if ($errors->has('neighborhood'))
            <span>
                <strong>{{ $errors->first('neighborhood') }}</strong>
            </span>
            @endif
        </div>
        
        <div class="formgroup col col-g spacing {{ $errors->has('cep') ? ' is-invalid' : '' }}">
            <label>CEP</label>
            <input type="text" name="cep" value="{{ old('cep') ?? $comprador->cep }}" required="" maxlength="9" placeholder="Ex. 66666-666" max="255" onkeyup="mascara(this, mcep);">

            @if ($errors->has('cep'))
            <span>
                <strong>{{ $errors->first('cep') }}</strong>
            </span>
            @endif
        </div>

    </div>



    <div class="row">

        <div class="formgroup col col-x spacing {{ $errors->has('email') ? ' is-invalid' : '' }}">
            <label>E-MAIL</label>
            <input type="email" name="email" value="{{ old('email') ?? $comprador->email }}" placeholder="Ex. user@example.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">

            @if ($errors->has('email'))
            <span>
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>

        <div class="formgroup col col-x spacing {{ $errors->has('phone_number') ? ' is-invalid' : '' }}">
            <label>TELEFONE 1</label>
            <input type="tel" name="phone_number" value="{{ old('phone_number') ?? $comprador->phone_number }}" max="255" placeholder="Ex: (99) 99999-9999" onkeyup="mascara(this, mtel);"  maxlength="15" pattern="\([0-9]{2}\) [0-9]{4,6}-[0-9]{3,4}$" >

            @if ($errors->has('phone_number'))
            <span>
                <strong>{{ $errors->first('phone_number') }}</strong>
            </span>
            @endif
        </div>

    </div>


    <div class="row">

        <div class="formgroup col col-x spacing {{ $errors->has('bank') ? ' is-invalid' : '' }}">
            <label>BANCO</label>
            <input type="text" name="bank" value="{{ old('bank') ?? $comprador->bank }}" required="" placeholder="Ex. Banco do Brasil">

            @if ($errors->has('bank'))
            <span>
                <strong>{{ $errors->first('bank') }}</strong>
            </span>
            @endif
        </div>

        <div class="formgroup col col-x spacing {{ $errors->has('agency') ? ' is-invalid' : '' }}">
            <label>AGÊNCIA</label>
            <input type="text" name="agency" value="{{ old('agency') ?? $comprador->agency }}" required="" placeholder="Ex. 8847">

            @if ($errors->has('agency'))
            <span>
                <strong>{{ $errors->first('agency') }}</strong>
            </span>
            @endif
        </div>

        <div class="formgroup col col-x spacing {{ $errors->has('account') ? ' is-invalid' : '' }}">
            <label>CONTA</label>
            <input type="text" name="account" value="{{ old('account') ?? $comprador->account }}" required="" max="255" placeholder="Ex. 88475-8">

            @if ($errors->has('account'))
            <span>
                <strong>{{ $errors->first('account') }}</strong>
            </span>
            @endif
        </div>

        <div class="formgroup col col-x spacing {{ $errors->has('account_type') ? ' is-invalid' : '' }}">
            <label>TIPO DE CONTA</label>

            <select name="account_type">
                <option value="cc" {{ $comprador->account_type == 'cc' ? 'selected' : '' }}>Conta corrente</option>
                <option value="pp" {{ $comprador->account_type == 'pp' ? 'selected' : '' }}>Poupança</option>
                <option value="ff" {{ $comprador->account_type == 'ff' ? 'selected' : '' }}>Conta facil</option>
            </select>

        </div>

    </div>



    <div class="formgroup">
        <button type="submit" class="btn fl-left" >CADASTRAR</button>
        <button type="reset" class="btn btn-danger fl-right">LIMPAR</button>
    </div>
</form>

@endsection