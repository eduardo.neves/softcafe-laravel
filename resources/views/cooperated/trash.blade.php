@extends('layouts.admin')

@section('content')

<h1>Lixeira (Cooperados)</h1>

@if(session()->has('message'))
<div class="alert alert-success">
    <i class="fa fa-info-circle"></i> {{ session()->get('message') }}
</div>
@endif

<table class="dataTable striped">
    
    <thead>
        <tr>
            <th>Nome</th>
            <th>CPF</th>
            <th>Endereço</th>
            <th>Telefone</th>
            <th>Ações</th>
        </tr>
        
    </thead>
    
    <tbody>
        
        @foreach($cooperados as $cooperado)
        <tr>
            <td><a href="{{ route('cooperated.show', $cooperado->id) }}">{{ $cooperado->name }}</a></td>
            <td>{{ $cooperado->cpf }}</td>
            <td>{{ strip_tags(substr($cooperado->address, 0, 20)) . " [...]" }}</td>
            <td>{{ $cooperado->phone_number }}</td>
            <td>
                <a href="{{ route('cooperated.restore', $cooperado->id) }}" class="btn btn-success"><i class="fa fa-refresh"></i></a>
                <a href="{{ route('cooperated.destroy', $cooperado->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
        @endforeach
        
    </tbody>
</table>

@endsection