@extends('layouts.admin')

@section('content')

<h1>Novo cooperado</h1>

@if(session()->has('message'))
<div class="alert alert-success">
    <i class="fa fa-info-circle"></i> {{ session()->get('message') }}
</div>
@endif

<form action="{{ route('cooperated.create') }}" method="POST" enctype="multipart/form-data">

    @csrf

    <div class="formgroup {{ $errors->has('name') ? ' is-invalid' : '' }}">
        <label>NOME COMPLETO</label>
        <input type="text" name="name" value="{{ old('name') }}" required="" placeholder="Nome completo" max="255" autofocus="">

        @if ($errors->has('name'))
        <span>
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>

    <div class="formgroup {{ $errors->has('address') ? ' is-invalid' : '' }}">
        <label>ENDEREÇO</label>
        <input type="text" name="address" value="{{ old('address') }}" placeholder="Ex: Pass. Toledo, 18, Pedreira" required="">

        @if ($errors->has('address'))
        <span>
            <strong>{{ $errors->first('address') }}</strong>
        </span>
        @endif
    </div>

    <div class="row">

        <div class="formgroup col col-x spacing {{ $errors->has('cpf') ? ' is-invalid' : '' }}">
            <label>CPF</label>
            <input type="text" name="cpf" value="{{ old('cpf') }}" placeholder="Ex: 999.999.999-99" maxlength="14" required="" pattern="\d{3}\.\d{3}\.\d{3}-\d{2}" onkeyup="mascara(this, mcpf);">

            @if ($errors->has('cpf'))
            <span>
                <strong>{{ $errors->first('cpf') }}</strong>
            </span>
            @endif
        </div>

        <div class="formgroup col col-x spacing {{ $errors->has('phone_number') ? ' is-invalid' : '' }}">
            <label>TELEFONE 1</label>
            <input type="tel" name="phone_number" value="{{ old('phone_number') }}" max="255" placeholder="Ex: (99) 99999-9999" onkeyup="mascara(this, mtel);"  maxlength="15" pattern="\([0-9]{2}\) [0-9]{4,6}-[0-9]{3,4}$" >

            @if ($errors->has('phone_number'))
            <span>
                <strong>{{ $errors->first('phone_number') }}</strong>
            </span>
            @endif
        </div>

    </div>

    <div class="row">

        <div class="formgroup col col-x spacing {{ $errors->has('email') ? ' is-invalid' : '' }}">
            <label>E-MAIL</label>
            <input type="email" name="email" value="{{ old('email') }}" placeholder="Ex. user@example.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">

            @if ($errors->has('email'))
            <span>
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>

        <div class="formgroup col col-x spacing {{ $errors->has('sex') ? ' is-invalid' : '' }}">
            <label>SEXO</label>
            <select name="sex" required="">
                <option value="m">Masculino</option>
                <option value="f">Feminino</option>
            </select>
        </div>

    </div>

    <div class="row">

        <div class="formgroup col col-x spacing {{ $errors->has('born_date') ? ' is-invalid' : '' }}">
            <label>DATA DE NASCIMENTO</label>
            <input type="date" name="born_date" value="{{ old('born_date') }}" required="" placeholder="DATA DE NASCIMENTO">

            @if ($errors->has('born_date'))
            <span>
                <strong>{{ $errors->first('born_date') }}</strong>
            </span>
            @endif
        </div>

        <div class="formgroup col col-x spacing {{ $errors->has('profile_photo') ? ' is-invalid' : '' }}"">
            <label>FOTO DE PERFIL</label>
            <input type="file" name="profile_photo" accept="image/*">

            @if ($errors->has('profile_photo'))
            <span>
                <strong>{{ $errors->first('profile_photo') }}</strong>
            </span>
            @endif
        </div>

    </div>



    <div class="formgroup">
        <button type="submit" name="new-copperated" class="btn fl-left" >CADASTRAR</button>
        <button type="reset" name="new-copperated" class="btn btn-danger fl-right">LIMPAR</button>
    </div>
</form>

@endsection