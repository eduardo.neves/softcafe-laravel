@extends('layouts.admin')

@section('content')

<h1 class="al-center">{{ $cooperado->name }}</h1>

<div class="cooperado">

    <h2 class="al-center">Dados pessoais</h2>

    <div class="cooperado-dado coop row">

        <div class="col col-x spacing"><strong class="fl-right">IMAGEM</strong></div> <div class="col col-x spacing"><img src="/img/cooperated_images/{{ $cooperado->profile_photo == null ? 'noimage.png' : $cooperado->profile_photo }}"></div>
        <div class="col col-x spacing"><strong class="fl-right">ENDEREÇO</strong></div> <div class="col col-x spacing">{{ $cooperado->address }}</div>
        <div class="col col-x spacing"><strong class="fl-right">CPF</strong></div> <div class="col col-x spacing">{{ $cooperado->cpf }}</div>
        <div class="col col-x spacing"><strong class="fl-right">TELEFONE</strong></div> <div class="col col-x spacing">{{ $cooperado->phone_number }}</div>
        <div class="col col-x spacing"><strong class="fl-right">EMAIL</strong></div> <div class="col col-x spacing">{{ $cooperado->email }}</div>
        <div class="col col-x spacing"><strong class="fl-right">SEXO</strong></div> <div class="col col-x spacing">{{ $cooperado->sex == 'm' ? 'Masculino' : 'Feminino' }}</div>
        <div class="col col-x spacing"><strong class="fl-right">DATA DE NASCIMENTO</strong></div> <div class="col col-x spacing">{{ date('d/m/Y', strtotime($cooperado->born_date)) }}</div>
    </div>

    <h2 class="al-center">Dados bancarios</h2>

    <div class="cooperado-banco coop row">

        @if($cooperado->bankData->first() == null)
        <p>Nenhum dado bancario</p>
        @else

        <div class="col col-x spacing"><strong class="fl-right">BANCO</strong></div> <div class="col col-x spacing">{{ $cooperado->bankData->first()->bank }}</div>
        <div class="col col-x spacing"><strong class="fl-right">AGENCIA</strong></div> <div class="col col-x spacing">{{ $cooperado->bankData->first()->agency }}</div>
        <div class="col col-x spacing"><strong class="fl-right">CONTA</strong></div> <div class="col col-x spacing">{{ $cooperado->bankData->first()->account }}</div>
        <div class="col col-x spacing"><strong class="fl-right">TIPO DE CONTA</strong></div> <div class="col col-x spacing">
            @switch($cooperado->bankData->first()->account_type)
                @case('cc')
                Conta corrente
                @break

                @case('pp')
                Polpança
                @break
                
                @case('ss')
                Salário
                @break

                @case('ff')
                Conta fácil
                @break
                
                @default
                Default case...
            @endswitch
        </div>

        @endif


    </div>

    <h2 class="al-center">Ultimas vendas</h2>

    <table>
        <thead>
            <tr>
                <th>MÊS</th>
                <th>NUMERO DE SACAS</th>
                <th>VALOR ARRECADADO</th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td>Julho</td>
                <td>60</td>
                <td>R$ 1389,00</td>
            </tr>

            <tr>
                <td>Julho</td>
                <td>60</td>
                <td>R$ 1389,00</td>
            </tr>

            <tr>
                <td>Julho</td>
                <td>60</td>
                <td>R$ 1389,00</td>
            </tr>

            <tr>
                <td>Julho</td>
                <td>60</td>
                <td>R$ 1389,00</td>
            </tr>

            <tr>
                <td>Julho</td>
                <td>60</td>
                <td>R$ 1389,00</td>
            </tr>
        </tbody>
    </table>
</div>

@endsection