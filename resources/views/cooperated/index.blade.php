@extends('layouts.admin')

@section('content')

<h1>Cooperados</h1>


@if(session()->has('message'))
<div class="alert alert-success">
    <i class="fa fa-info-circle"></i> {{ session()->get('message') }}
</div>
@endif

@if(session()->has('danger'))
<div class="alert alert-warning">
    <i class="fa fa-warning"></i> {{ session()->get('danger') }}
</div>
@endif

<table class="dataTable striped">

    <thead>
        <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>CPF</th>
            <th>Endereço</th>
            <th>Telefone</th>
            <th>Saldo</th>
            <th></th>
        </tr>

    </thead>

    <tbody>

        @foreach($cooperados as $cooperado)
        <tr>
            <td>{{ $cooperado->id }}</td>
            <td class="table-link"><a href="{{ route('cooperated.show', $cooperado->id) }}">{{ $cooperado->name }}</a></td>
            <td>{{ $cooperado->cpf }}</td>
            <td>{{ strip_tags(substr($cooperado->address, 0, 20)) . " [...]" }}</td>
            <td>{{ $cooperado->phone_number }}</td>
            @if($cooperado->stocks->first() == null)
            <td class="table-link"><a href="{{ route('stock.addstock', $cooperado->id) }}">0 [add+]</a></td>
            @else 
            <td class="table-link"><a href="{{ route('stock.editstock', $cooperado->id) }}">{{ $cooperado->stocks->first()->bags }} SC [<i class="fa fa-pencil"></i>]</a></td>
            @endif
            
            <td>
                <a href="{{ route('cooperated.delete', $cooperado->id) }}" class="btn btn-warning"><i class="fa fa-trash"></i></a>
                <a href="{{ route('cooperated.edit', $cooperado->id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
            </td>
        </tr>
        @endforeach

    </tbody>
</table>

@endsection