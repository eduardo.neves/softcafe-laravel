@extends('layouts.app')

@section('content')

<section class="auth row">

    <div class="wrap-g">

        <h1>Acesse sua conta</h1>
        
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="formgroup">

                <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="E-mail" name="email" value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>

            <div class="formgroup">

                <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Senha" name="password" required>

                @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>

            <div class="formgroup">
                <label><input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} > Manter online</label>
            </div>

            <div class="formgroup">
                <button type="submit" class="btn btn-warning"><i class="fa fa-lock"></i>Entrar</button>

                <a class="btn btn-link" href="{{ route('password.request') }}">Esqueceu a senha?</a>
            </div>
        </form>

    </div>

</section>


@endsection
