@extends('layouts.admin')

@section('content')

<h1>Vendas</h1>

@if(session()->has('message'))
<div class="alert alert-success">
    <i class="fa fa-info-circle"></i> {{ session()->get('message') }}
</div>
@endif

<table class="dataTable striped">

    <thead>
        <tr>
            <th>COMPRADOR</th>
            <th>SACAS</th>
            <th>VALOR</th>
            <th>DATA</th>
            <th></th>
        </tr>

    </thead>

    <tbody>

        @foreach($vendas as $venda)
        <tr>
            <td>{{ $venda->buyer->name }}</a></td>
            <td>{{ $venda->bags }}</a></td>
            <td>R${{ number_format($venda->sale_value, 2, ',', '.') }}</td>
            <td>{{ date('d/m/Y', strtotime($venda->created_at)) }}</td>
            <td>
                <a href="{{ route('sale.delete', $venda->id) }}" class="btn btn-warning"><i class="fa fa-trash"></i></a>
                <a href="{{ route('sale.edit', $venda->id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
            </td>
        </tr>
        @endforeach

    </tbody>
</table>

@endsection