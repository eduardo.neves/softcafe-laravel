@extends('layouts.admin')

@section('content')

<h1>Nova venda</h1>

@if(session()->has('message'))
<div class="alert alert-success">
    <i class="fa fa-info-circle"></i> {{ session()->get('message') }}
</div>
@endif



<form action="{{ route('sale.create') }}" method="POST" enctype="multipart/form-data">

    @csrf

    <input name="sale_value" class="sale_value" type="hidden" value="1" class="buyer_id"> 
    
    <div class="formgroup buyer_wrap {{ $errors->has('buyer_id') ? ' is-invalid' : '' }}">
        
        <label>COMPRADOR</label>
        
        <input name="buyer" type="text" class="buyer" placeholder="Pesquisar..." required="" autocomplete="off"> 
        
        <div class="venda"></div>
    </div>

    <div class="formgroup {{ $errors->has('email') ? ' is-invalid' : '' }}">
        <label>NUMERO DE SACAS</label>
        <input type="number" class="bags" name="bags" required="" value="{{ old('bags') }}" placeholder="Ex. 556">

        @if ($errors->has('bags'))
        <span>
            <strong>{{ $errors->first('bags') }}</strong>
        </span>
        @endif
    </div>

    <div class="formgroup {{ $errors->has('sale_value') ? ' is-invalid' : '' }}">
        <label>VALOR TOTAL</label>
        <input type="text" disabled="" class="total" name="sale_value" value="R$00,00">

        @if ($errors->has('sale_value'))
        <span>
            <strong>{{ $errors->first('sale_value') }}</strong>
        </span>
        @endif
    </div>


    <div class="formgroup">
        <button type="submit" class="btn fl-left" >CADASTRAR</button>
        <button type="reset" class="btn btn-danger fl-right">LIMPAR</button>
    </div>
</form>

@endsection