@extends('layouts.admin')

@section('content')

<h1>Novo usuário</h1>

@if(session()->has('message'))
<div class="alert alert-success">
    <i class="fa fa-info-circle"></i> {{ session()->get('message') }}
</div>
@endif

<form action="{{ route('user.create') }}" method="POST" enctype="multipart/form-data">

    @csrf

    <div class="formgroup {{ $errors->has('name') ? ' is-invalid' : '' }}">
        <label>NOME</label>
        <input type="text" name="name" value="{{ old('name') }}" required="" placeholder="Nome completo" max="255" autofocus="">

        @if ($errors->has('name'))
        <span>
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>

    <div class="formgroup {{ $errors->has('email') ? ' is-invalid' : '' }}">
        <label>E-MAIL</label>
        <input type="email" name="email" value="{{ old('email') }}" placeholder="Ex. user@example.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">

        @if ($errors->has('email'))
        <span>
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
    </div>

    <div class="formgroup {{ $errors->has('password') ? ' is-invalid' : '' }}">
        <label>SENHA</label>
        <input type="password" name="password" placeholder="Minimo de 6 digitos" min="6">

        @if ($errors->has('password'))
        <span>
            <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
    </div>

    <div class="formgroup">
        <label for="password-confirm">CONFIRMA SENHA</label>

        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirma senha" required>
    </div>



    <div class="formgroup">
        <button type="submit" name="new-copperated" class="btn fl-left" >CADASTRAR</button>
        <button type="reset" name="new-copperated" class="btn btn-danger fl-right">LIMPAR</button>
    </div>
</form>

@endsection