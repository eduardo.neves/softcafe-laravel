@extends('layouts.admin')

@section('content')

<h1>Novo usuário</h1>

@if(session()->has('message'))
<div class="alert alert-success">
    <i class="fa fa-info-circle"></i> {{ session()->get('message') }}
</div>
@endif

<form action="{{ route('user.update', $user->id) }}" method="POST" enctype="multipart/form-data">

    @csrf

    <div class="formgroup {{ $errors->has('name') ? ' is-invalid' : '' }}">
        <label>NOME</label>
        <input type="text" name="name" value="{{ old('name') ?? $user->name }}" required="" placeholder="Nome completo" max="255" autofocus="">

        @if ($errors->has('name'))
        <span>
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>


    <div class="formgroup">
        <button type="submit" class="btn fl-left">ATUALIZAR</button>
        <button type="reset" class="btn btn-danger fl-right">LIMPAR</button>
    </div>
</form>

@endsection