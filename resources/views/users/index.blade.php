@extends('layouts.admin')

@section('content')

<h1>Usuários</h1>

@if(session()->has('message'))
<div class="alert alert-success">
    <i class="fa fa-info-circle"></i> {{ session()->get('message') }}
</div>
@endif

<table class="dataTable striped">

    <thead>
        <tr>
            <th>Nome</th>
            <th>EMAIL</th>
            <th>CRIAÇÃO</th>
            <th></th>
        </tr>

    </thead>

    <tbody>

        @foreach($usuarios as $usuario)
        <tr>
            <td class="table-link"><a href="{{ route('user.show', $usuario->id) }}">{{ $usuario->name }}</a></td>
            <td>{{ $usuario->email }}</td>
            <td>{{ date('d/m/Y', strtotime($usuario->created_at)) }}</td>
            <td>
                <a href="{{ route('user.delete', $usuario->id) }}" class="btn btn-warning"><i class="fa fa-trash"></i></a>
                <a href="{{ route('user.edit', $usuario->id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
            </td>
        </tr>
        @endforeach

    </tbody>
</table>

@endsection