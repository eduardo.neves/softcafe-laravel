<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <meta charset="UTF-8">        

        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

        <meta name="description" content="Projeto de construção de uma aplicação web voltada para a automatização das cooperativas de café na versão atual 0.1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <meta name="theme-color" content="#3e2723" />
        <meta name="robots" content="all,follow">

        <title>Softcafe</title>

        <link rel="shortcut icon" href="/favicon.png">

        <link rel="stylesheet" href="/css/boot.css">
        <link rel="stylesheet" href="/css/fa/fontawesome.css">
        <link rel="stylesheet" href="/css/style.css">




    </head>

    <body>

        <nav class="menu row">

            <div class="wrap-g">

                <h1><a href="{{ route('home') }}">Softcafe</a></h1>

                <a class="btn-menu-toggle"><i class="fa fa-bars"></i></a>

                <div class="menu-container"></div>

                <ul class="menu-main">
                    <a class="btn-menu-toggle"><i class="fa fa-angle-left"></i></a>

                    <li>
                        <a class="ancor"><i class="fa fa-users"></i>Cooperados<i class="fa fa-angle-right"></i></a>

                        <ul>
                            <a class="btn-close">Cooperados<i class="fa fa-angle-left"></i></a>
                            <li><a href="{{ route('cooperated.index') }}">Cooperados</a></li>
                            <li><a href="{{ route('cooperated.create') }}">Novo cooperado</a></li>
                            <li><a href="{{ route('cooperated.trash') }}">Lixeira</a></li>
                        </ul>
                    </li>

                    <li>
                        <a class="ancor"><i class="fa fa-user"></i>Usuários<i class="fa fa-angle-right"></i></a>

                        <ul>
                            <a class="btn-close">Usuários<i class="fa fa-angle-left"></i></a>
                            <li><a href="{{ route('user.index') }}">Usuários</a></li>
                            <li><a href="{{ route('user.create') }}">Novo usuário</a></li>
                            <li><a href="{{ route('user.trash') }}">Lixeira</a></li>
                        </ul>
                    </li>

                    <li>
                        <a class="ancor"><i class="fa fa-calculator"></i>Estoque<i class="fa fa-angle-right"></i></a>

                        <ul>
                            <a class="btn-close">Estoque<i class="fa fa-angle-left"></i></a>
                            <li><a href="{{ route('stock.index') }}">Estoques</a></li>
                            <li><a href="{{ route('stock.trash') }}">Lixeira</a></li>
                        </ul>

                    </li>

                    <li>
                        <a class="ancor"><i class="fa fa-user"></i>Compradores<i class="fa fa-angle-right"></i></a>

                        <ul>
                            <a class="btn-close">Compradores<i class="fa fa-angle-left"></i></a>
                            <li><a href="{{ route('buyer.index') }}">Compradores</a></li>
                            <li><a href="{{ route('buyer.create') }}">Novo comprador</a></li>
                            <li><a href="{{ route('buyer.negativeds') }}">Negativados</a></li>
                            <li><a href="{{ route('buyer.trash') }}">Lixeira</a></li>
                        </ul>

                    </li>

                    <li>
                        <a class="ancor"><i class="fa fa-dollar"></i>Vendas<i class="fa fa-angle-right"></i></a>

                        <ul>
                            <a class="btn-close">Vendas<i class="fa fa-angle-left"></i></a>
                            <li><a href="{{ route('sale.index') }}">Vendas</a></li>
                            <li><a href="{{ route('sale.create') }}">Nova venda</a></li>
                            <li><a href="{{ route('sale.trash') }}">Canceladas</a></li>
                            <li><a>Relatorios</a></li>
                        </ul>
                    </li>

                    <li>
                        <a class="ancor"><i class="fa fa-gears"></i>Configurações<i class="fa fa-angle-right"></i></a>

                        <ul>
                            <a class="btn-close">Configurações<i class="fa fa-angle-left"></i></a>
                            <li><a>Sobre</a></li>
                        </ul>
                    </li>

                    <li><a class="out" href="{{ route('logout') }}"><i class="fa fa-sign-out"></i>Sair</a></li>
                </ul>
            </div>

        </nav>

        <main class="section row">
            <div class="wrap-g">

                @yield('content')

            </div>
        </main>



    </body>

    <script src="/js/jquery.js"></script>
    <script src="/js/dataTable.min.js"></script>
    <script src="/js/mascara.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="/js/chart.js"></script>
    <script src="/js/script.js"></script>
    

</html>    
