<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <meta charset="UTF-8">        

        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

        <meta name="description" content="Projeto de construção de uma aplicação web voltada para a automatização das cooperativas de café na versão atual 0.1">
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
        <meta name="theme-color" content="#3e2723" />
        <meta name="robots" content="all,follow">

        <title>Softcafe</title>

        <link rel="shortcut icon" href="favicon.png">

        <link rel="stylesheet" href="/css/boot.css">
        <link rel="stylesheet" href="/css/fa/fontawesome.css">
        <link rel="stylesheet" href="/css/style.css">

    </head>

    <body>
        
        @yield('content')
        
    </body>

    <script src="js/jquery.js"></script>
    <script src="js/script.js"></script>
    <script src="js/mascara.js"></script>
</html>    
