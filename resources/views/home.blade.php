@extends('layouts.admin')

@section('content')

<h1>Bem vindo</h1>

<div class="home row">


    <a href="{{ route('cooperated.index') }}" class="cooperados col col-m spacing bg-red-lighten">Cooperados</a>
    <a href="{{ route('user.index') }}" class="usuarios col col-m spacing bg-blue-lighten">Usuarios</a>
    <a href="{{ route('stock.index') }}" class="estoque col col-m spacing bg-green-lighten">Estoque</a>
    <a href="{{ route('buyer.index') }}" class="compradores col spacing col-m bg-orange-lighten">Compradores</a>

</div>

<div class="grafic row">

    <h2>Crescimento das vendas em 2018</h2>

    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>

@endsection
