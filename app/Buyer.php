<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    protected $fillable = [
        'name', 'address', 'city', 'neighborhood', 'cep', 'email', 'cpf', 'cnpj', 'phone_number',  'bank', 'agency', 'account', 'account_type'
    ];
    
    public function sales() {
        return $this->hasMany('App\Sale');
    }
}
