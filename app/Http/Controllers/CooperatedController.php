<?php

namespace App\Http\Controllers;

use App\Cooperated;
use Illuminate\Support\Str;
use Image;

class CooperatedController extends Controller
{
    
    function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cooperateds = Cooperated::where('active', 1)->orderBy('created_at', 'desc')->get();
        return view('cooperated.index')->with('cooperados', $cooperateds);
    }
    
    /**
     * 
     * @return type
     */
    public function trash() {
        $cooperateds = Cooperated::where('active', 0)->get();
        return view('cooperated.trash')->with('cooperados', $cooperateds);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cooperated.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'name' => 'required|string|max:255',
            'cpf' => 'required|string|max:255|unique:cooperateds',
            'address' => 'required|string|max:255',
            'phone_number' => 'max:255',
            'email' => 'max:255|unique:cooperateds',
            'sex' => 'required|string|max:255',
            'born_date' => 'required|max:255',
        ]);

        if (request()->hasFile('profile_photo')) {

            $this->validate(request(), [
                'profile_photo' => 'image:jpg, png, jpeg|mimes:jpg,png,jpeg|max:2048'
            ]);
        }
        
        $cooperated = Cooperated::create(request()->all());
        
//        if (request()->hasFile('profile_photo')) {
//
//            $uploadedFile = request()->file('profile_photo');        
//            $fileName = 'cooperated_' . $cooperated->id . '_' . Str::random(5) . '.' . $uploadedFile->getClientOriginalExtension();
//            $location = public_path('img/cooperated_images/' . $fileName);
//            Image::make($uploadedFile->getRealPath())->resize(360, 360, function ($constraint) {$constraint->aspectRatio();})->save($location);
//            $cooperated->profile_photo = $fileName;
//            $cooperated->save();
//        } 

        return redirect()->back()->with('message', 'Cooperado cadastrado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $co = Cooperated::find($id);
        return view('cooperated.show')->with('cooperado', $co);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $co = Cooperated::find($id);
        
        return view('cooperated.edit')->with('cooperado', $co);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $co = Cooperated::find($id)->update(request()->all());
        return redirect()->back()->with('message', 'Cooperado atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $co = Cooperated::find($id);
        $co->active = false;
        $co->save();
        
        return back()->with('message', 'Cooperado enviado para a lixeira');
    }
    
    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id) {
        
        $co = Cooperated::find($id);
        $co->active = true;
        $co->save();
        
        return back()->with('message', 'Cooperado restaurado com sucesso');        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $co = Cooperated::find($id);
        $co->delete();
        
        return back()->with('message', 'Cooperado deletado com sucesso');
    }
}
