<?php

namespace App\Http\Controllers;

use App\Stock;
use App\Cooperated;

class StockController extends Controller
{
    function __construct() {
        $this->middleware('auth');
    }
    
    public function index() {
        
        $stock = Stock::where('active', 1)->orderBy('created_at', 'desc')->get();
        
        return view('stocks.index')->with('stocks', $stock);
    }
    
    public function addstock($id) {
        
        $co = \App\Cooperated::find($id);
        
        return view('stocks.new-stock')->with('cooperado', $co);
    }
    
    public function storestock() {
        
        $this->validate(request(), [
            'bags' => 'required|numeric',
            'type' => 'required|string',
        ]);
        
        Stock::create(request()->all());
        
        return back()->with('message', 'Estoque cadstrado com sucesso');
    }
    
    public function editstock($id) {
        
        $this->validate(request(), [
            'bags' => 'required|numeric',
            'type' => 'required|string',
        ]);
        
        $st = Stock::where('cooperated_id', $id)->get();
        
        if(request('bags') == 0){
            $st->first()->delete();
            
            return back()->with('message', 'Stoque atualizado com sucesso');
        }
        
        $st->first()->update(request()->all());
        
        return back()->with('message', 'Stoque atualizado com sucesso');
        
    }
    
    public function trash() {
        $stock = Stock::where('active', 0)->orderBy('created_at', 'desc')->get();
        
        return view('stocks.trash')->with('stocks', $stock);
    }
    
    public function delete($id) {
        
        $stock = Stock::find($id);
        
        $stock->active = false;
        $stock->save();
        
        return back()->with('message', 'Enviado para a lixeira');
        
        
    }
    
    public function destroy($id) {
        
        $stock = Stock::find($id);
        
        $stock->delete();
        
        return back()->with('message', 'Deletado com sucesso');
    }
    
    public function restore($id) {
        
        $stock = Stock::find($id);
        
        $stock->active = true;
        $stock->save();
        
        return back()->with('message', 'Restaurado com sucesso');
    }
    
    public function showeditstock($id) {
        
        $co = Cooperated::find($id);
        
        if($co->stocks->first() == null){
            
            return redirect()->route('cooperated.index')->with('message', 'O cooperado não possue mais estoque');
        }
        
        return view('stocks.edit')->with('cooperado', $co);
    }

}
