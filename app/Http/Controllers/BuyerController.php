<?php

namespace App\Http\Controllers;

use App\Buyer;

class BuyerController extends Controller {

    function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $buyers = Buyer::where('active', 1)->where('status', 1)->orderBy('created_at', 'desc')->get();
        return view('buyers.index')->with('buyers', $buyers);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function negativeds() {
        $buyers = Buyer::where('active', 1)->where('status', 0)->orderBy('created_at', 'desc')->get();
        return view('buyers.negativeds')->with('buyers', $buyers);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function negative($id) {
        $b = Buyer::find($id);
        $b->status = false;
        $b->save();
        return redirect()->back()->with('message', 'Comprador está negativado!');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function positive($id) {
        $b = Buyer::find($id);
        $b->status = true;
        $b->save();
        return redirect()->back()->with('message', 'Comprador está positivo!');
    }

    /**
     * 
     * @return type
     */
    public function trash() {
        $buyers = Buyer::where('active', 0)->get();
        return view('buyers.trash')->with('buyers', $buyers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('buyers.create');
    }
    
    public function verify() {
        
        $this->validate(request(), [
            'name' => 'required|string|max:255', 
            'address' => 'required|string|max:255', 
            'city' => 'required|string|max:255', 
            'neighborhood' => 'required|string|max:255', 
            
            'cpf' => 'required|string|max:14', 
            'cnpj' => 'required|string|max:18', 
            'phone_number' => 'required|string|max:15', 
            'bank' => 'required|string|max:255', 
            'agency' => 'required|string|max:255', 
            'account' => 'required|string|max:255', 
            'account_type' => 'required|string|max:255' 

        ]);

        if (request()->has('cep')) {

            $this->validate(request(), [
                'cep' => 'required|string|max:9'
            ]);
        }
        
        if (request()->has('email')) {

            $this->validate(request(), [
                'email' => 'max:255|unique:buyers'
            ]);
        }
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store() {
        
        $this->verify();

        Buyer::create(request()->all());

        return redirect()->back()->with('message', 'Comprador cadastrado com sucesso!');
    }

    /**
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $bu = Buyer::find($id);
        return view('buyers.show')->with('comprador', $bu);
    }

    /**
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $bu = Buyer::find($id);

        return view('buyers.edit')->with('comprador', $bu);
    }

    /**
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id) {
        
        $this->validate(request(), [
            'name' => 'required|string|max:255', 
            'address' => 'required|string|max:255', 
            'city' => 'required|string|max:255', 
            'neighborhood' => 'required|string|max:255', 
            'phone_number' => 'required|string|max:15', 
            'bank' => 'required|string|max:255', 
            'agency' => 'required|string|max:255', 
            'account' => 'required|string|max:255', 
            'account_type' => 'required|string|max:255' 

        ]);

        if (request()->has('cep')) {

            $this->validate(request(), [
                'cep' => 'required|string|max:9'
            ]);
        }
        
        if (request()->has('email')) {

            $this->validate(request(), [
                'email' => 'max:255|nullable|unique:buyers'
            ]);
        }
        
        Buyer::find($id)->update(request()->all());
        return back()->with('message', 'Comprador atualizado com sucesso!');
    }

    /**
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id) {
        $co = Buyer::find($id);
        $co->active = false;
        $co->save();

        return back()->with('message', 'Cooperado enviado para a lixeira');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id) {

        $co = Buyer::find($id);
        $co->active = true;
        $co->save();

        return back()->with('message', 'Cooperado restaurado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $co = Buyer::find($id);
        $co->delete();

        return back()->with('message', 'Cooperado deletado com sucesso');
    }

}
