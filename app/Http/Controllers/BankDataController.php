<?php

namespace App\Http\Controllers;

use App\Cooperated;
use App\BankData;


class BankDataController extends Controller
{
    
    function __construct() {
        $this->middleware('auth');
    }

    
    public function showaddbank($id) {
        
        $co = Cooperated::find($id);
        return view('cooperated.new-bank')->with('cooperado', $co);
        
    }
    
    public function storebank() {
        
        $this->validate(request(), [
            'bank' => 'required|string|max:255',
            'agency' => 'required|string|max:255',
            'account' => 'required|string|max:255',
            'account_type' => 'required|string|max:255',
        ]);
        
        BankData::create(request()->all());
        
        return back()->with('message', 'Dados bancarios cadastrados com sucesso');
        
    }
}
