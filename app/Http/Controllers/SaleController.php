<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sale;
use App\Buyer;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = Sale::where('active', 1)->orderBy('created_at', 'desc')->get();
        return view('sales.index')->with('vendas', $sales);
    }
    
    public function getb() {
        
        if(empty(request('search'))){
            return [];
        }
        $buyers = Buyer::where('name', 'ILIKE', '%' . request('search') . '%')->where('active', 1)->where('status', 1)->orderBy('created_at', 'desc')->get();
        return response()->json($buyers, 200, [], JSON_PRETTY_PRINT);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sales.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'bags' => 'required|numeric',
            'sale_value' => 'required|numeric',
        ]);
        
        $b = Buyer::where('name', request('buyer'))->get();
        $b = $b->first();
        
        if($b == null){
            return back()->with('message', 'Usuário não encontrado');
        }
        
        $sale = Sale::create([
            'bags' => request('bags'),
            'sale_value' => request('sale_value'),
            'buyer_id' => $b->id
        ]);
        
        return back()->with('message', 'Venda cadastrada com sucesso');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $s = Sale::find($id);
        return view('sales.edit')->with('sale', $s);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [
            'bags' => 'required|numeric',
            'sale_value' => 'required|numeric',
        ]);
        
        $b = Buyer::where('name', request('buyer'))->get();
        $b = $b->first();
        
        if($b == null){
            return back()->with('message', 'Usuário não encontrado');
        }
        
        $sale = Sale::find($id)->update([
            'bags' => request('bags'),
            'sale_value' => request('sale_value'),
            'buyer_id' => $b->id
        ]);
        
        return back()->with('message', 'Venda atualizada com sucesso');
    }
    
    public function trash() {
        
        $sales = Sale::where('active', 0)->orderBy('created_at', 'desc')->get();
        return view('sales.trash')->with('vendas', $sales);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sale = Sale::find($id);
        $sale->delete();
        
        return back()->with('message', 'Venda deletada');
    }
    
    public function delete($id) {
        
        $s = Sale::find($id);
        $s->active = false;
        $s->save();
        
        return back()->with('message', 'Venda cancelada');
    }
    
    public function restore($id) {
        
        $s = Sale::find($id);
        $s->active = true;
        $s->save();
        
        return back()->with('message', 'Venda restaurada');
    }
}
