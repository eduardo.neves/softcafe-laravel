<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    
    function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('active', 1)->orderBy('created_at', 'desc')->get();
        return view('users.index')->with('usuarios', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        
        User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => Hash::make(request('password')),
        ]);
        
        return back()->with('message', 'Usuário cadastrado com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show')->with('usuario', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('users.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [
            'name' => 'required|string|max:255',
        ]);
        
        User::find($id)->update(request()->all());
        
        return back()->with('message', 'Usuário atualizado com sucesso');
    }
    
    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $user = User::find($id);
        $user->active = true;
        $user->save();
        return back()->with('message', 'Usuário restaurado com sucesso');
    }
    
    /**
     * Show inactive users
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function trash()
    {
        $users = User::where('active', 0)->orderBy('created_at', 'desc')->get();
        return view('users.index')->with('usuarios', $users);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if(auth()->user()->email == 'user@example.com'){
            return back()->with('message', 'Não pode excluir o usuário master');
        }
        if(auth()->user()->id == $id){
            return back()->with('message', 'Você precisa logar com outro usuário');
        }
        $user = User::find($id);
        $user->active = false;
        $user->save();
        return back()->with('message', 'Usuário enviado para a lixeira');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(auth()->user()->email == 'user@example.com'){
            return back()->with('message', 'Não pode excluir o usuário master');
        }
        
        if(auth()->user()->id == $id){
            return back()->with('message', 'Você precisa logar com outro usuário');
        }
        
        $user = User::find($id);
        $user->delete();
        return back()->with('message', 'Usuário deletado com sucesso');
    }
}
