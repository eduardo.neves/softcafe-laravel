<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = [
        'bags', 'sale_value', 'buyer_id'
    ];
    
    public function buyer() {
        return $this->belongsTo('App\Buyer', 'buyer_id');
    }
}
