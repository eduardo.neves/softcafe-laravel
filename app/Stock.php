<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = ['bags', 'type', 'cooperated_id'];
    
    public function cooperated() {
        return $this->belongsTo('App\Cooperated', 'cooperated_id');
    }
}
