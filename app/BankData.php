<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankData extends Model
{
    protected $fillable = ['bank', 'agency', 'account', 'account_type', 'cooperated_id'];
    
    public function Cooperated() {
        return $this->belongsTo('App\Cooperated', 'cooperated_id');
    }
}
