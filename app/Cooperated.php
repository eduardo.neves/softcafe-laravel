<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cooperated extends Model
{
    protected $fillable = [
        'name',
        'cpf',
        'address',
        'phone_number',
        'email',
        'sex',
        'born_date'
    ];
    
    public function bankData() {
        return $this->hasMany('App\BankData');
    }
    
    public function stocks() {
        return $this->hasMany('App\Stock');
    }
}
