<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        // $this->call(UsersTableSeeder::class);

        DB::table('users')->insert([
            'name' => 'Eduardo Neves',
            'email' => 'user@example.com',
            'password' => Hash::make('123456')
        ]);

        $faker = Faker::create();

        foreach (range(1, 60) as $i) {

            DB::table('cooperateds')->insert([
                'name' => $faker->name,
                'cpf' => '000.000.000-' . ($i < 10 ? '0' . $i : $i),
                'address' => $faker->streetAddress,
                'phone_number' => '(91) 98888-8888',
                'email' => $faker->unique()->email,
                'sex' => 'm',
                'born_date' => '1989-12-23',
            ]);
        }
        
        foreach (range(1, 15) as $i) {

            DB::table('buyers')->insert([
                'name' => $faker->name . ' SA', 
                'address' => $faker->streetAddress, 
                'city' => 'Belem', 
                'neighborhood' => 'Pedreira', 
                'email' => $faker->unique()->email,
                'cep' => '66666-666',
                'cpf' => '000.000.000-' . ($i < 10 ? '0' . $i : $i), 
                'cnpj' => '45.797.431/5454-' . ($i < 10 ? '0' . $i : $i), 
                'phone_number' => '(91) 98888-8888', 
                'bank' => 'Banco do Brasil', 
                'agency' => '554488', 
                'account' => $i . '544454-1', 
                'account_type' => 'cc' 
            ]);
        }
    }

}
