<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buyers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address');
            $table->string('city');
            $table->string('neighborhood');
            $table->string('cep')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('cpf')->unique()->nullable();
            $table->string('cnpj')->unique();
            $table->string('phone_number')->nullable();
            $table->string('bank');
            $table->string('account');
            $table->string('agency');
            $table->string('account_type');
            $table->string('status')->default(true);
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buyers');
    }
}
